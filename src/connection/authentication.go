package connection

import (
	"errors"
	"fmt"

	"gitlab.com/stickman_0x00/go_psql/src/messages"
	"gitlab.com/stickman_0x00/go_psql/src/sasl"
)

func (me Conn) authentication(m messages.BMessage) error {
	authentication_type := m.Read_int32()

	switch authentication_type {
	case 0: // Specifies that the authentication was successful.Auth
		return nil
	case 10: // Specifies that SASL authentication is required.
		mechanism := m.Read_string()

		password, exist := me.dsn.User.Password()
		if !exist {
			return errors.New("no password passed on DSN")
		}

		sasl_auth, err := sasl.New(me.conn, me.dsn.User.Username(), password, mechanism)
		if err != nil {
			return fmt.Errorf("fail SSAL authentication: %w", err)
		}

		err = sasl_auth.Execute()
		if err != nil {
			return fmt.Errorf("fail SSAL authentication: %w", err)
		}
	// case 11: already handle in 10
	default:
		return errors.New("authentication type not implemented")
	}

	return nil
}
