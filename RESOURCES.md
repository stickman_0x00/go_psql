# Startup

[55.2.1. Start-up](https://www.postgresql.org/docs/current/protocol-flow.html#id-1.10.6.7.3)

# SASL

- [SCRAM-SHA-256 and SCRAM-SHA-256-PLUS - Simple Authentication and Security Layer (SASL) Mechanisms](https://www.rfc-editor.org/rfc/rfc7677)
- [Salted Challenge Response Authentication Mechanism (SCRAM) - SASL and GSS-API Mechanisms](https://www.rfc-editor.org/rfc/rfc5802)
- [Salted Challenge Response Authentication Mechanism](https://en.wikipedia.org/wiki/Salted_Challenge_Response_Authentication_Mechanism)
- [55.3. SASL Authentication](https://www.postgresql.org/docs/current/sasl-authentication.html)
