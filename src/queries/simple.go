package queries

import (
	"fmt"
	"net"

	log "gitlab.com/stickman_0x00/go_log"
	"gitlab.com/stickman_0x00/go_psql/src/messages"
	"gitlab.com/stickman_0x00/go_psql/src/statement"
)

// https://www.postgresql.org/docs/current/protocol-flow.html#id-1.10.6.7.4
func Simple(conn net.Conn, query string) (*statement.Stmt, error) {
	// Send query message
	_, err := conn.Write(messages.Query(query))
	if err != nil {
		return nil, fmt.Errorf("fail sending message Query: %v", err)
	}

	stmt := statement.New()

	for {
		received_message, err := messages.Decode(conn)
		if err != nil {
			// case messages.ERROR:
			return nil, fmt.Errorf("fail receiving message: %v", err)
		}

		switch received_message.Identifier() {
		case messages.COMMAND_COMPLETED:
		case messages.COPY_IN_RESPONSE:
			log.Warnln("not implement %v", received_message.Identifier())
		case messages.COPY_OUT_RESPONSE:
			log.Warnln("not implement %v", received_message.Identifier())
		case messages.ROW_DESCRIPTION:
			stmt.Add_row_description(received_message)
		case messages.DATA_ROW:
			stmt.Add_data_row(received_message)
		case messages.EMPTY_QUERY_RESPONSE:
			log.Warnln("not implement %v", received_message.Identifier())
		case messages.READY_FOR_QUERY:
			return stmt, nil
		case messages.NOTICE_RESPONSE:
			log.Warnln("not implement %v", received_message.Identifier())
		default:
			return nil, fmt.Errorf("simple query cycle has new possible response: %c", received_message.Identifier())
		}
	}
}
