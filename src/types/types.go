package psql_types

import (
	"strconv"

	log "gitlab.com/stickman_0x00/go_log"
)

type OID int

// https://www.postgresql.org/docs/current/datatype.html
// SELECT oid, typname FROM pg_type;
const (
	int4    OID = 23
	varchar OID = 1043
)

type convert_f func(value []byte) any

var convert map[OID]convert_f = map[OID]convert_f{
	int4:    int4_f,
	varchar: varchar_f,
}

func New(typ OID, value []byte) any {
	if value == nil {
		return nil
	}

	f, exist := convert[typ]
	if !exist {
		log.Warnf("data type oid not being handle: %d\n", typ)
		return value
	}

	return f(value)
}

func int4_f(value []byte) any {
	i, _ := strconv.Atoi(string(value))
	return i
}

func varchar_f(value []byte) any {
	return string(value)
}
