package queries

import (
	"database/sql/driver"
	"errors"
	"fmt"
	"net"

	"gitlab.com/stickman_0x00/go_psql/src/messages"
	"gitlab.com/stickman_0x00/go_psql/src/statement"
)

// https://www.postgresql.org/docs/current/protocol-flow.html#PROTOCOL-FLOW-EXT-QUERY
func Extended(conn net.Conn, query string, args []driver.NamedValue) (*statement.Stmt, error) {
	_, err := conn.Write(messages.Parse(query))
	if err != nil {
		return nil, fmt.Errorf("fail sending message Parse: %v", err)
	}

	_, err = conn.Write(messages.Bind(args))
	if err != nil {
		return nil, fmt.Errorf("fail sending message Bind: %v", err)
	}

	_, err = conn.Write(messages.Execute())
	if err != nil {
		return nil, fmt.Errorf("fail sending message Execute: %v", err)
	}

	_, err = conn.Write(messages.Sync())
	if err != nil {
		return nil, fmt.Errorf("fail sending message Sync: %v", err)
	}

	received_message, err := messages.Decode(conn)
	if err != nil {
		// case messages.ERROR:
		return nil, fmt.Errorf("fail receiving message: %v", err)
	}

	switch received_message.Identifier() {
	case messages.PARSE_COMPLETE:
	}

	return nil, errors.New("not implemented")
}
