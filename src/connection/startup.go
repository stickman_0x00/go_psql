package connection

import (
	"fmt"
	"net"

	"gitlab.com/stickman_0x00/go_psql/src/messages"
)

func (me *Conn) startup() error {
	var err error

	// Open connection to server
	me.conn, err = net.Dial("tcp", me.dsn.Host)
	if err != nil {
		return fmt.Errorf("fail connecting to server: %w", err)
	}

	// Send startup message
	_, err = me.conn.Write(messages.Startup_message(me.dsn.User.Username(), me.dsn.Path[1:]))
	if err != nil {
		return fmt.Errorf("fail sending startup_message: %w", err)
	}

	for {
		received_message, err := me.receive_message()
		if err != nil {
			return fmt.Errorf("fail receiving message: %w", err)
		}

		switch received_message.Identifier() {
		case messages.AUTHENTICATION_REQUEST:
			err = me.authentication(received_message)
			if err != nil {
				return fmt.Errorf("authentication failed: %w", err)
			}

		case messages.PARAMETER_STATUS:
		case messages.BACKEND_KEY_DATA:
		case messages.READY_FOR_QUERY:
			return nil
		default:
			return fmt.Errorf("message type not implemented %c", received_message.Identifier())
		}
	}
}
