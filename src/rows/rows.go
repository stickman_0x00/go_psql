package rows

import (
	"database/sql/driver"
	"io"

	log "gitlab.com/stickman_0x00/go_log"
	"gitlab.com/stickman_0x00/go_psql/src/messages"
	psql_types "gitlab.com/stickman_0x00/go_psql/src/types"
)

type field_description struct {
	Table_oid     int
	Index         int
	Type_oid      psql_types.OID
	Type_size     int
	Type_modifier int
	Format        int
}

// https://pkg.go.dev/database/sql/driver#Rows
type Rows struct {
	index       int
	fields_name []string
	fields      []field_description
	data        [][]any
}

func New() *Rows {
	return &Rows{
		index:       0,
		fields_name: []string{},
		fields:      []field_description{},
		data:        [][]any{},
	}
}

// Receive RowDescription (B)
func (me *Rows) Add_fields_description(message messages.BMessage) {
	n_fields := message.Read_int16()

	for i := 0; i < n_fields; i++ {
		me.fields_name = append(me.fields_name, message.Read_string())

		field := field_description{
			Table_oid:     message.Read_int32(),
			Index:         message.Read_int16(),
			Type_oid:      psql_types.OID(message.Read_int32()),
			Type_size:     message.Read_int16(),
			Type_modifier: message.Read_int32(),
			Format:        message.Read_int16(),
		}

		me.fields = append(me.fields, field)
	}
}

// DataRow (B)
func (me *Rows) Add_data_row(message messages.BMessage) {
	n_values := message.Read_int16()

	data_row := []any{}
	for i := 0; i < n_values; i++ {
		length := message.Read_int32()

		if length == -1 {
			data_row = append(data_row, nil)
			continue
		}

		data_row = append(data_row, psql_types.New(me.fields[i].Type_oid, message.Read_n(length)))

	}

	me.data = append(me.data, data_row)
}

func (me Rows) Columns() []string {
	log.Traceln("rows.Columns()")

	return me.fields_name
}

func (me Rows) Close() error {
	log.Traceln("rows.Close()")
	return nil
}

func (me *Rows) Next(dest []driver.Value) error {
	log.Traceln("rows.Next()")

	if me.index == len(me.data) {
		return io.EOF
	}

	for i := range dest {
		dest[i] = me.data[me.index][i]
	}

	me.index++

	return nil
}
