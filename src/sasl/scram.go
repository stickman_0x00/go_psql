package sasl

import (
	"crypto/hmac"
	"crypto/sha256"
	"encoding/base64"
	"fmt"
)

func (me *SASL) client_proof() []byte {
	// SaltedPassword  := Hi(Normalize(password), salt, i)
	salted_password := hi(me.password, me.salt, me.iter_count)

	//  ClientKey       := HMAC(SaltedPassword, "Client Key")
	h := hmac.New(sha256.New, salted_password)
	h.Write([]byte("Client Key"))
	client_key := h.Sum(nil)

	//  StoredKey       := H(ClientKey)
	h = sha256.New()
	h.Write(client_key)
	stored_key := h.Sum(nil)

	//  AuthMessage     := client-first-message-bare + "," +
	//                     server-first-message + "," +
	//                     client-final-message-without-proof
	client_final_message_without_proof := fmt.Sprintf("c=biws,r=%s", me.server_nonce)
	me.auth_message = append(me.auth_message, ',')
	me.auth_message = append(me.auth_message, []byte(client_final_message_without_proof)...)

	//  ClientSignature := HMAC(StoredKey, AuthMessage)
	h = hmac.New(sha256.New, stored_key)
	h.Write(me.auth_message)
	client_signature := h.Sum(nil)

	//  ClientProof     := ClientKey XOR ClientSignature
	client_proof := xor(client_key, client_signature)

	client_proof_64 := make([]byte, base64.StdEncoding.EncodedLen(len(client_proof)))
	base64.StdEncoding.Encode(client_proof_64, client_proof)

	return client_proof_64
}

func (me SASL) server_signature() []byte {
	// SaltedPassword  := Hi(Normalize(password), salt, i)
	salted_password := hi(me.password, me.salt, me.iter_count)

	//ServerKey       := HMAC(SaltedPassword, "Server Key")
	h := hmac.New(sha256.New, salted_password)
	h.Write([]byte("Server Key"))
	server_key := h.Sum(nil)

	// ServerSignature := HMAC(ServerKey, AuthMessage)
	h = hmac.New(sha256.New, server_key)
	h.Write(me.auth_message)
	server_signature := h.Sum(nil)

	server_signature_64 := make([]byte, base64.StdEncoding.EncodedLen(len(server_signature)))
	base64.StdEncoding.Encode(server_signature_64, server_signature)

	return server_signature_64
}

func hi(str string, salt []byte, iter_count int) []byte {
	b_str := []byte(str)

	// U1   := HMAC(str, salt + INT(1))
	U1 := hmac.New(sha256.New, b_str) // str
	U1.Write(salt)                    // salt
	U1.Write([]byte{0, 0, 0, 1})      // INT(1) // TODO:

	// U2   := HMAC(str, U1)
	Ui := U1.Sum(nil)[:]

	hi := Ui[:]
	for i := 1; i < iter_count; i++ {
		U1.Reset()   // str
		U1.Write(Ui) // Ui-2
		Ui = U1.Sum(nil)[:]

		hi = xor(hi, Ui)
	}

	return hi
}

func xor(b1 []byte, b2 []byte) []byte {
	for i, b := range b2 {
		b1[i] ^= b
	}

	return b1
}
