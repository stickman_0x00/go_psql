package postgresql

import (
	"fmt"
)

type Config struct {
	user     string
	password string

	host string
	port int

	db string

	sslmode string
}

func New_config(user string, db string) Config {
	return Config{
		user:     user,
		password: "",
		host:     "localhost",
		port:     5432,
		db:       db,
		sslmode:  "required",
	}
}

func (me *Config) Set_password(password string) {
	me.password = password
}

func (me *Config) Set_sslmode(mode string) {
	me.sslmode = mode
}

// Output data source name
// (34.1.1.2. Connection URIs)[https://www.postgresql.org/docs/current/libpq-connect.html]
func (me Config) DSN() string {
	str := "postgresql://"

	// userspec
	str += me.user

	if me.password != "" {
		str += ":" + me.password
	}

	str += "@"

	// hostspec
	str += fmt.Sprintf("%s:%d/%s", me.host, me.port, me.db)

	// paramspec
	str += "?sslmode=" + me.sslmode

	return str
}
