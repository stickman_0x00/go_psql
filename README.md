# Postgresql driver in GO

# Start test DB

```bash
docker-compose -f docker/docker-compose.yml up -d
```

# Test

```bash
go run src/cmd/main.go
```