package sasl

import (
	"bytes"
	"crypto/rand"
	"encoding/base64"
	"errors"
	"fmt"
	"net"
	"strconv"

	"gitlab.com/stickman_0x00/go_psql/src/messages"
)

type SASL struct {
	username  string
	password  string
	mechanism string

	client_nonce []byte
	server_nonce []byte
	auth_message []byte
	salt         []byte
	iter_count   int

	conn net.Conn
}

func New(conn net.Conn, username string, password string, mechanism string) (SASL, error) {

	if mechanism != "SCRAM-SHA-256" {
		return SASL{}, fmt.Errorf("mechanism not implement: %s", mechanism)
	}

	me := SASL{
		username:  username,
		password:  password,
		mechanism: mechanism,

		client_nonce: []byte{},
		server_nonce: []byte{},
		auth_message: []byte{},
		iter_count:   0,

		conn: conn,
	}

	return me, nil
}

func (me *SASL) Execute() error {
	err := me.step_1()
	if err != nil {
		return fmt.Errorf("fail on step_1: %w", err)
	}

	err = me.step_2()
	if err != nil {
		return fmt.Errorf("fail on step_2: %w", err)
	}

	err = me.step_3()
	if err != nil {
		return fmt.Errorf("fail on step_3: %w", err)
	}

	err = me.step_4()
	if err != nil {
		return fmt.Errorf("fail on step_4: %w", err)
	}

	return nil
}

// Send initial message
func (me *SASL) step_1() error {
	me.client_nonce = make([]byte, 16)
	_, err := rand.Read(me.client_nonce)
	if err != nil {
		return fmt.Errorf("fail generating nonce: %w", err)
	}

	client_initial_response_bare := fmt.Sprintf("n=%s,r=%s", me.username, base64.RawStdEncoding.EncodeToString(me.client_nonce))
	me.auth_message = append(me.auth_message, []byte(client_initial_response_bare)...)

	client_initial_response := fmt.Sprintf("n,,%s", client_initial_response_bare)

	_, err = me.conn.Write(messages.SASL_initial_response(me.mechanism, client_initial_response))
	if err != nil {
		return fmt.Errorf("fail sending client initial response: %w", err)
	}

	return nil
}

// Receive server first message
func (me *SASL) step_2() error {
	server_response, err := messages.Decode(me.conn)
	if err != nil {
		return fmt.Errorf("fail receiving response AuthenticationSASLContinue: %w", err)
	}

	type_message := server_response.Read_int32()
	if type_message != 11 {
		return fmt.Errorf("expected SASL challenge got: %d", type_message)
	}

	server_first_message := server_response.Read_bytes()
	me.auth_message = append(me.auth_message, ',')
	me.auth_message = append(me.auth_message, server_first_message...)

	fields := bytes.Split(server_first_message, []byte{','})

	me.server_nonce = fields[0][2:] // nonce

	// salt
	me.salt = make([]byte, base64.StdEncoding.DecodedLen(len(fields[1][2:])))
	n, err := base64.StdEncoding.Decode(me.salt, fields[1][2:])
	if err != nil {
		return fmt.Errorf("cannot decode SCRAM-SHA-256 salt sent by server: %q", fields[1])
	}
	me.salt = me.salt[:n]

	me.iter_count, err = strconv.Atoi(string(fields[2][2:])) // i
	if err != nil {
		return fmt.Errorf("fail getting i: %w", err)
	}

	return nil
}

// Send client final message
func (me *SASL) step_3() error {
	client_proof := me.client_proof()
	client_final_message := fmt.Sprintf("c=biws,r=%s,p=%s", me.server_nonce, client_proof)

	_, err := me.conn.Write(messages.SASL_response(client_final_message))
	if err != nil {
		return fmt.Errorf("failed sending SASL response: %w", err)
	}

	return nil
}

// Receive server last message and confirm signature
func (me SASL) step_4() error {
	server_response, err := messages.Decode(me.conn)
	if err != nil {
		return fmt.Errorf("fail receiving response AuthenticationSASLFinal: %w", err)
	}

	type_message := server_response.Read_int32()
	if type_message != 12 {
		return fmt.Errorf("expected the authentication to be complete, got: %d", type_message)
	}

	v := server_response.Read_bytes()[2:]

	server_signature := me.server_signature()

	if !bytes.Equal(v, server_signature) {
		return errors.New("server signature received is different")
	}

	return nil
}
