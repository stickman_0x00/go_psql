package postgresql

import (
	"database/sql"
	"database/sql/driver"

	log "gitlab.com/stickman_0x00/go_log"
	"gitlab.com/stickman_0x00/go_psql/src/connection"
)

type Driver struct{}

func init() {
	sql.Register("postgres", &Driver{})
}

func (me *Driver) Open(name string) (driver.Conn, error) {
	log.Trace("driver.Open()")
	return connection.New(name)
}
