-- Creation of user table
CREATE TABLE IF NOT EXISTS users (
	user_id INT NOT NULL,
	name varchar(250) NOT NULL,
	age INT,

	PRIMARY KEY (user_id)
);