package messages

import (
	"bytes"
	"encoding/binary"
)

func (me *BMessage) Read_string() string {
	index := bytes.IndexByte(me.content, 0)
	s := me.content[:index]
	me.content = me.content[index+1:] // +1 because of "\000"

	return string(s)
}

func (me *BMessage) Read_int16() int {
	i := int16(binary.BigEndian.Uint16(me.content[:2]))
	me.content = me.content[2:]
	return int(i)
}

func (me *BMessage) Read_int32() int {
	i := int32(binary.BigEndian.Uint32(me.content[:4]))
	me.content = me.content[4:]
	return int(i)
}

func (me *BMessage) Read_byte() byte {
	b := me.content[0]
	me.content = me.content[1:]

	return b
}

func (me *BMessage) Read_n(n int) []byte {
	b := me.content[:n]
	me.content = me.content[n:]

	return b
}

func (me *BMessage) Read_bytes() []byte {
	defer func() {
		me.content = []byte{}
	}()

	return me.content
}

func (me *FMessage) reserve_length() {
	me.add_int_32(0)
}

func (me *FMessage) add_zero_byte() {
	me.content = append(me.content, 0)
}

func (me *FMessage) add_string(s string) {
	me.content = append(me.content, []byte(s)...)
	me.add_byte(0)
}

func (me *FMessage) add_int_16(i int) {
	me.content = binary.BigEndian.AppendUint16(me.content, uint16(i))
}

func (me *FMessage) add_int_32(i int) {
	me.content = binary.BigEndian.AppendUint32(me.content, uint32(i))
}

func (me *FMessage) add_byte(b byte) {
	me.content = append(me.content, b)
}

func (me *FMessage) add_bytes(b []byte) {
	me.content = append(me.content, b...)
}

func (me *FMessage) payload() []byte {
	// write length
	position_of_length := length_positions[me.typ]
	content_length := len(me.content[position_of_length:])
	binary.BigEndian.PutUint32(me.content[position_of_length:], uint32(content_length))

	return me.content
}
