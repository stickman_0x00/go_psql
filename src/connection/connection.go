package connection

import (
	"context"
	"database/sql/driver"
	"fmt"
	"net"
	"net/url"

	log "gitlab.com/stickman_0x00/go_log"
	"gitlab.com/stickman_0x00/go_psql/src/queries"
)

type Conn struct {
	conn net.Conn

	dsn *url.URL // Data source name
}

func New(dsn string) (driver.Conn, error) {
	me := &Conn{}

	var err error

	me.dsn, err = url.Parse(dsn)
	if err != nil {
		return nil, fmt.Errorf("failed parsing dsn: %w", err)
	}

	err = me.startup()
	if err != nil {
		return nil, err
	}

	return me, nil
}

func (me *Conn) Prepare(query string) (driver.Stmt, error) {
	panic("Conn.Prepare() substituted by Conn.QueryContext")
}

// https://pkg.go.dev/database/sql/driver#QueryerContext
func (me Conn) QueryContext(ctx context.Context, query string, args []driver.NamedValue) (driver.Rows, error) {
	log.Traceln("Conn.QueryContext()", args)

	if len(args) == 0 {
		stmt, err := queries.Simple(me.conn, query)
		if err != nil {
			return nil, err
		}

		return stmt.Rows, nil
	}

	stmt, err := queries.Extended(me.conn, query, args)
	if err != nil {
		return nil, err
	}

	return stmt.Rows, nil
}

func (me *Conn) Close() error {
	return me.conn.Close()
}

// Deprecated
func (me *Conn) Begin() (driver.Tx, error) {
	panic("Conn.Beging() deprecated")
}
