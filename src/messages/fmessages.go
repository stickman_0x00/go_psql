package messages

import (
	"database/sql/driver"
	"fmt"
)

// https://www.postgresql.org/docs/current/protocol-message-formats.html

type message_type byte

type FMessage struct {
	typ     message_type
	content []byte
}

func new_fmessage(typ message_type) *FMessage {
	return &FMessage{
		typ:     typ,
		content: make([]byte, 0),
	}
}

// Bind (F)
func Bind(args []driver.NamedValue) []byte {
	me := new_fmessage(BIND)

	me.add_byte('B')

	me.reserve_length()

	me.add_string("")

	me.add_string("")

	me.add_int_16(0)

	// me.add_int_16()

	len_args := len(args)
	me.add_int_16(len_args)

	for i := 0; i < len_args; i++ {
		v := fmt.Sprintf("%v", args[i].Value)
		me.add_int_32(len(v))

		me.add_bytes([]byte(v))
	}

	me.add_int_16(0)

	// me.add_int_16()

	return me.payload()
}

// Execute (F)
func Execute() []byte {
	me := new_fmessage(EXECUTE)

	me.add_byte('E')

	me.reserve_length()

	me.add_string("")

	me.add_int_32(0)

	return me.payload()
}

// Parse (F)
func Parse(query string) []byte {
	me := new_fmessage(PARSE)

	me.add_byte('P')

	me.reserve_length()

	me.add_string("")

	me.add_string(query)

	me.add_int_16(0)

	// me.add_int_32(0)

	return me.payload()
}

// StartupMessage (F)
func Startup_message(user string, DB string) []byte {
	me := new_fmessage(STARTUP_MESSAGE)

	me.reserve_length()

	me.add_int_32(196608) // Version

	// Parameters
	me.add_string("user")
	me.add_string(user)

	me.add_string("database")
	me.add_string(DB)

	me.add_zero_byte()

	return me.payload()
}

// SASLInitialResponse (F)
func SASL_initial_response(mechanism string, client_initial_response string) []byte {
	me := new_fmessage(SASL_INITIAL_RESPONSE)

	me.add_byte('p') // Identifier

	me.reserve_length()

	me.add_string(mechanism) // Name of the SASL authentication mechanism

	me.add_int_32(len(client_initial_response)) // Length of SASL mechanism

	me.add_bytes([]byte(client_initial_response)) // Initial Client response

	return me.payload()
}

// SASLResponse (F)
func SASL_response(client_final_message string) []byte {
	me := new_fmessage(SASL_RESPONSE)

	me.add_byte('p')

	me.reserve_length()

	me.add_bytes([]byte(client_final_message))

	return me.payload()
}

// SYNC (F)
func Sync() []byte {
	me := new_fmessage(SYNC)

	me.add_byte('S')

	me.reserve_length()

	return me.payload()
}

// Query (F)
func Query(query string) []byte {
	me := new_fmessage(QUERY)

	me.add_byte('Q')

	me.reserve_length()

	me.add_string(query)

	return me.payload()
}
