package main

import (
	"database/sql"
	"fmt"
	"log"

	psql "gitlab.com/stickman_0x00/go_psql/src"
)

// Simple test

const (
	user     string = "test"
	password string = "test"
	db_name  string = "test"
)

func main() {
	config := psql.New_config(user, db_name)
	config.Set_password(password)
	config.Set_sslmode("disable")
	dsn := config.DSN()

	db, err := sql.Open("postgres", dsn)
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	// rows, err := db.Query("SELECT * FROM users;")
	rows, err := db.Query("SELECT * FROM users WHERE age = $1 AND name =$2;", 21, "ol")
	if err != nil {
		log.Fatalln(err)
	}

	var id int
	var name string
	var age sql.NullInt64
	for rows.Next() {
		err := rows.Scan(&id, &name, &age)
		if err != nil {
			log.Fatalln(err)
		}
		fmt.Println(id, name, age)
	}
}
