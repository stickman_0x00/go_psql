package messages

import (
	"encoding/binary"
	"fmt"
	"io"
	"net"
)

type BMessage struct {
	identifier Message_identifier
	content    []byte
}

func Decode(conn net.Conn) (BMessage, error) {
	header := make([]byte, 5)
	_, err := io.ReadFull(conn, header)
	if err != nil {
		return BMessage{}, err
	}

	length := int(binary.BigEndian.Uint32(header[1:])) - 4

	me := BMessage{
		identifier: Message_identifier(header[0]),
		content:    make([]byte, length),
	}

	_, err = io.ReadFull(conn, me.content)
	if err != nil {
		return BMessage{}, err
	}

	if me.identifier == ERROR {
		return BMessage{}, me.get_error()
	}

	return me, nil
}

// [55.8. Error and Notice Message Fields](https://www.postgresql.org/docs/current/protocol-error-fields.html)
func (me BMessage) get_error() error {
	var message string
	var detail string

	for len(me.content) > 1 {
		field_type := me.Read_byte()
		value := me.Read_string()

		switch field_type {
		case 'M':
			message = value
		case 'D':
			detail = value
		}
	}

	return fmt.Errorf("%s: %s", message, detail)
}
