package messages

const (
	// Bind (F)
	BIND message_type = iota
	// Execute (F)
	EXECUTE
	// Parse (F)
	PARSE
	// StartupMessage (F)
	STARTUP_MESSAGE
	SASL_INITIAL_RESPONSE
	SASL_RESPONSE
	// SYNC (F)
	SYNC
	QUERY
)

var length_positions = map[message_type]int{
	BIND:                  1,
	EXECUTE:               1,
	PARSE:                 1,
	STARTUP_MESSAGE:       0,
	SASL_INITIAL_RESPONSE: 1,
	SASL_RESPONSE:         1,
	SYNC:                  1,
	QUERY:                 1,
}
