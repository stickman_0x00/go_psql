package connection

import "gitlab.com/stickman_0x00/go_psql/src/messages"

func (me *Conn) receive_message() (messages.BMessage, error) {
	return messages.Decode(me.conn)
}
