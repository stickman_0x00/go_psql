package statement

import (
	"context"
	"database/sql/driver"
	"errors"

	log "gitlab.com/stickman_0x00/go_log"
	"gitlab.com/stickman_0x00/go_psql/src/messages"
	"gitlab.com/stickman_0x00/go_psql/src/rows"
)

// https://pkg.go.dev/database/sql/driver#Stmt
type Stmt struct {
	n_fields int
	Rows     *rows.Rows
}

func New() *Stmt {
	return &Stmt{
		n_fields: 0,
		Rows:     rows.New(),
	}
}

// Receive RowDescription (B)
func (me Stmt) Add_row_description(message messages.BMessage) {
	me.Rows.Add_fields_description(message)
}

// DataRow (B)
func (me Stmt) Add_data_row(message messages.BMessage) {
	me.Rows.Add_data_row(message)
}

func (me Stmt) Close() error {
	log.Trace("stmt.Close()")
	return nil
}

func (me Stmt) NumInput() int {
	log.Trace("stmt.NumInput()")

	return 0
}

// Deprecated
func (me Stmt) Exec(args []driver.Value) (driver.Result, error) {
	panic("Stmt.Exec() deprecated")
}

func (me Stmt) ExecContext(ctx context.Context, args []driver.NamedValue) (driver.Result, error) {
	log.Traceln("stmt.ExecContext()", ctx, args)
	return nil, errors.New("not implemented")
}

// Deprecated
func (me Stmt) Query(args []driver.Value) (driver.Rows, error) {
	panic("Stmt.Query() deprecated")
}

func (me Stmt) QueryContext(ctx context.Context, args []driver.NamedValue) (driver.Rows, error) {
	log.Traceln("stmt.QueryContext()", args)
	return me.Rows, nil
}
