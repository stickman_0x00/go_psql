package messages

type Message_identifier byte

const (
	// CommandComplete (B)
	COMMAND_COMPLETED Message_identifier = 'C'
	// DataRow (B)
	DATA_ROW               Message_identifier = 'D'
	ERROR                  Message_identifier = 'E'
	COPY_IN_RESPONSE       Message_identifier = 'G'
	COPY_OUT_RESPONSE      Message_identifier = 'H'
	EMPTY_QUERY_RESPONSE   Message_identifier = 'I'
	BACKEND_KEY_DATA       Message_identifier = 'K'
	NOTICE_RESPONSE        Message_identifier = 'N'
	AUTHENTICATION_REQUEST Message_identifier = 'R'
	PARAMETER_STATUS       Message_identifier = 'S'
	// ParseComplete (B)
	PARSE_COMPLETE Message_identifier = '1'
	// RowDescription (B)
	ROW_DESCRIPTION Message_identifier = 'T'
	READY_FOR_QUERY Message_identifier = 'Z'
)

func (me BMessage) Identifier() Message_identifier {
	return me.identifier
}
